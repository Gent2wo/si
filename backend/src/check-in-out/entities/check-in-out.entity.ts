import { User } from "src/users/entities/user.entity";
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm"

@Entity()
export class CheckInOut {
    @PrimaryGeneratedColumn({
        name: 'CHECKINOUT_ID'
    })
    id: number;
    @Column({
        name: 'CHECKINOUT_TIMEIN'
    })
    timeIn: Date;
    @Column({
        name: 'CHECKINOUT_TIMEOUT',
        nullable: true,
        default: () => 'NULL',
    })
    timeOut?: Date | null;
    @Column({
        name: 'CHECKINOUT_STATUS'
    })
    status: string;
    @Column({
        name: 'CHECKINOUT_TOTALHOUR'
    })
    totalHour: number;
    @Column({
        name: 'CHECKINOUT_DATE'
    })
    date: Date;

    @ManyToOne(() => User, (user) => user.checkInOut)
    user: User;
}
