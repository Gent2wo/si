import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { CheckInOutService } from './check-in-out.service';
import { CreateCheckInOutDto } from './dto/create-check-in-out.dto';
import { UpdateCheckInOutDto } from './dto/update-check-in-out.dto';

@Controller('checkInOuts')
export class CheckInOutController {
  constructor(private readonly checkInOutService: CheckInOutService) { }

  @Post()
  create(@Body() createCheckInOutDto: CreateCheckInOutDto) {
    return this.checkInOutService.create(createCheckInOutDto);
  }

  @Get()
  findAll() {
    return this.checkInOutService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.checkInOutService.findOne(+id);
  }

  // @Get('user/:userId')
  // findAllByEmailPassword(@Param('userId') email: string, password: string) {
  //   return this.checkInOutService.findAllByEmailPassword(email, password);
  // }

  @Patch(':id')
  update(@Param('id') id: string,
    @Body() updateCheckInOutDto: UpdateCheckInOutDto) {
    return this.checkInOutService.update(+id, updateCheckInOutDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.checkInOutService.remove(+id);
  }
}
