import { Test, TestingModule } from '@nestjs/testing';
import { CheckInOutController } from './check-in-out.controller';
import { CheckInOutService } from './check-in-out.service';

describe('CheckInOutController', () => {
  let controller: CheckInOutController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CheckInOutController],
      providers: [CheckInOutService],
    }).compile();

    controller = module.get<CheckInOutController>(CheckInOutController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
