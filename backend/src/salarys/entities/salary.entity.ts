import { Branch } from 'src/branches/entities/branch.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({ name: 'SALARY' })
export class Salary {
  @PrimaryGeneratedColumn({
    name: 'SLR_ID',
  })
  id: number;

  @Column({
    name: 'SLR_FULLNAME',
  })
  fullName: string;

  @Column({
    name: 'SLR_MONTH',
  })
  month: string;

  @Column({
    name: 'SLR_PRICEHOUR',
  })
  pricePerHour: number;

  @Column({
    name: 'SLR_TOTALPAY',
  })
  totalPay: number;

  @Column({
    name: 'SLR_TOTALHOUR',
  })
  totalHour: number;

  @Column({
    name: 'SLR_STATUS',
  })
  status: string;

  @Column({
    name: 'SLR_PAYDATE',
  })
  payDate: Date;

  @CreateDateColumn({
    name: 'SLR_CREATED',
  })
  created: Date;

  @UpdateDateColumn({
    name: 'SLR_UPDATED',
  })
  updated: Date;

  @OneToMany(() => User, (user) => user.salary)
  @JoinColumn({ name: 'EMP_ID' })
  users: User[];

  @ManyToOne(() => Branch, (branch) => branch.salarys)
  @JoinColumn({ name: 'BRANCH_ID' })
  branch: Branch;
}
