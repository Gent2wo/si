export class CreateSalaryDto {
  fullName: string;
  pricePerHour: number;
  status: string;
  totalPay: number;
  totalHour: number;
  payDate: Date;
  month : string;
}
