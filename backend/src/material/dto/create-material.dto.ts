export class CreateMaterialDto {
  name: string;

  minimum: number;

  amount: number;

  type: string;
}
