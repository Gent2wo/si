import { Test, TestingModule } from '@nestjs/testing';
import { StockDetailController } from './stockDetail.controller';
import { StockDetailService } from './stockDetail.service';

describe('UsersController', () => {
  let controller: StockDetailController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [StockDetailController],
      providers: [StockDetailService],
    }).compile();

    controller = module.get<StockDetailController>(StockDetailController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
