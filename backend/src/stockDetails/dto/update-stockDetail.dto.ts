import { PartialType } from '@nestjs/mapped-types';
import { CreateStockDetailDto } from './create-stockDetail.dto';

export class UpdateStockDetailDto extends PartialType(CreateStockDetailDto) {}
