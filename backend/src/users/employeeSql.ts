import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class EmployeeSql {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  async createGetByBrachandfilterProcedure() {
    try {
        // Check if the procedure exists
        const existingProcedure = await this.userRepository.query(`SHOW PROCEDURE STATUS WHERE Name = 'getByBrachandfilter';`);
        // If the procedure exists, drop it
        if (existingProcedure.length > 0) {
            await this.userRepository.query(`DROP PROCEDURE getByBrachandfilter;`);
            console.log("Dropped getByBrachandfilter procedure");
        } else {
            console.log("No getByBrachandfilter procedure found to drop");
        }
        // Create the new procedure
        await this.userRepository.query(`
        CREATE PROCEDURE getByBrachandfilter(
            IN role_param VARCHAR(255), 
            IN sex_param VARCHAR(255),
            IN branch_param VARCHAR(255),
            IN status_param VARCHAR(255),
            IN min_salary_param INT,
            IN max_salary_param INT,
            IN min_age_param INT, 
            IN max_age_param INT
        )
        BEGIN
            SELECT 
                EMPLOYEE.EMP_ID, EMPLOYEE.EMP_NAME, EMPLOYEE.EMP_GENDER, EMPLOYEE.EMP_AGE, EMPLOYEE.EMP_BIRTHDATE, EMPLOYEE.EMP_PHONE,
                EMPLOYEE.EMP_SALARY, EMPLOYEE.EMP_PHOTO, EMPLOYEE.EMP_STATUS, EMPLOYEE.EMP_STARTDATE, EMPLOYEE.EMP_CREATED,
                EMPLOYEE.EMP_UPDATED, BRANCH.BRANCH_ID, BRANCH.BRANCH_ADDRESS, BRANCH.BRANCH_CITY,
                BRANCH.BRANCH_PROVINCE, BRANCH.BRANCH_COUNTRY, BRANCH.BRANCH_POSTAL_CODE, BRANCH.BRANCH_CREATED,
                BRANCH.BRANCH_UPDATED, BRANCH.BRANCH_NAME, ROLE.ROLE_ID, ROLE.ROLE_NAME,
                (SELECT COUNT(*) FROM EMPLOYEE 
                 WHERE (sex_param = 'all' OR EMPLOYEE.EMP_GENDER = sex_param) 
                   AND (role_param = 'all' OR ROLE.ROLE_NAME = role_param) 
                   AND (branch_param = 'all' OR BRANCH.BRANCH_NAME = branch_param)
                   AND (status_param = 'all' OR EMPLOYEE.EMP_STATUS = status_param)
                   AND (min_salary_param <= EMPLOYEE.EMP_SALARY AND max_salary_param >= EMPLOYEE.EMP_SALARY)
                   AND EMPLOYEE.EMP_AGE BETWEEN min_age_param AND max_age_param) AS EMPLOYEE_COUNT
            FROM EMPLOYEE
            INNER JOIN BRANCH ON EMPLOYEE.BRANCH_ID = BRANCH.BRANCH_ID
            LEFT JOIN ROLE ON EMPLOYEE.ROLE_ID = ROLE.ROLE_ID
            WHERE (sex_param = 'all' OR EMPLOYEE.EMP_GENDER = sex_param) 
              AND (role_param = 'all' OR ROLE.ROLE_NAME = role_param) 
              AND (branch_param = 'all' OR BRANCH.BRANCH_NAME = branch_param)
              AND (status_param = 'all' OR EMPLOYEE.EMP_STATUS = status_param)
              AND (min_salary_param <= EMPLOYEE.EMP_SALARY AND max_salary_param >= EMPLOYEE.EMP_SALARY)
              AND (min_age_param <= EMPLOYEE.EMP_AGE AND max_age_param >= EMPLOYEE.EMP_AGE);
        END;
               
        `);
        console.log("Created getByBrachandfilter procedure successfully");
    } catch (error) {
        throw new Error("Failed to create getByBrachandfilter procedure");
    }
}


}
