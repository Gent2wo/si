import { PartialType } from '@nestjs/mapped-types';
import { CreateUserDto } from './create-user.dto';

export class UpdateUserDto extends PartialType(CreateUserDto) {
  //Information
  name: string;
  gender: string;
  height: string;
  weight: string;
  bloodType: string;
  age: string;
  birthDate: string;

  //Contact
  phone: string;
  email: string;
  address: string;

  //work
  role: string;
  startDate: string;
  status: string;
  salary: string;
  branch: string;

  //Login user Email and Password
  password: string;

  //photo
  image: string;
}
