import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Product } from './entities/product.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ProductSql {
  constructor(
    @InjectRepository(Product)
    private readonly productRepository: Repository<Product>,
  ) {}

  async createGetProductByCategoryProcedure() {
    try {
      // Check if the procedure exists
      const existingProcedure = await this.productRepository.query(
        `SHOW PROCEDURE STATUS WHERE Name = 'getProductByCategory';`,
      );
      // If the procedure exists, drop it
      if (existingProcedure.length > 0) {
        await this.productRepository.query(`DROP PROCEDURE getProductByCategory;`);
        console.log('Dropped getProductByCategory procedure');
      } else {
        console.log('No getProductByCategory procedure found to drop');
      }
      // Create the new procedure
      await this.productRepository.query(`
        CREATE PROCEDURE getProductByCategory(IN pType INT)
BEGIN
    DECLARE num INT DEFAULT 0;
    DECLARE i INT DEFAULT 1;

    CREATE TEMPORARY TABLE IF NOT EXISTS temp(
        RowID INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
        PRODUCT_ID VARCHAR(50),
        PRODUCT_NAME VARCHAR(50),
        CATEGORY_NAME VARCHAR(50),
        ItemDetail VARCHAR(50)
    ) ENGINE = MEMORY;

    INSERT INTO temp (PRODUCT_ID, PRODUCT_NAME, CATEGORY_NAME)
    SELECT A.PRODUCT_ID, A.PRODUCT_NAME, B.CATEGORY_NAME 
    FROM PRODUCT A
    LEFT JOIN CATEGORY B ON A.CATEGORY_ID = B.CATEGORY_ID
    WHERE A.CATEGORY_ID = pType;

    SET num = (SELECT COUNT(*) FROM temp);

    WHILE i <= num DO
        UPDATE temp SET ItemDetail = CONCAT('Product No: ', i) WHERE RowID = i;
        SET i = i + 1;
    END WHILE;

    SELECT * FROM temp;

    DROP TEMPORARY TABLE IF EXISTS temp;
END //
;
               
        `);
      console.log('Created getProductByCategory procedure successfully');
    } catch (error) {
      throw new Error('Failed to create getProductByCategory procedure');
    }
  }
}
