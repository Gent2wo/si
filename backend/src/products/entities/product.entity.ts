import { Category } from 'src/category/entities/category.entity';
import { ReceiptDetail } from 'src/receipt/entities/receiptDetail.entity';

import { Size } from 'src/sizes/entities/size.entity';
import { SubCategory } from 'src/sub-categorys/entities/sub-category.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({
  name: 'PRODUCT',
})
export class Product {
  @PrimaryGeneratedColumn({
    name: 'PRODUCT_ID',
  })
  id: number;

  @Column({
    default: 'noimage.PNG',
    name: 'PRODUCT_IMG',
  })
  image: string;

  @Column({
    name: 'PRODUCT_NAME',
  })
  name: string;

  @Column({
    name: 'PRODUCT_PRICE',
  })
  price: number;

  @Column({
    name: 'CATEGORY_ID',
  })
  categoryId: number;

  @CreateDateColumn({
    name: 'PRODUCT_CREATED',
  })
  created: Date;

  @UpdateDateColumn({
    name: 'PRODUCT_UPDATED',
  })
  updated: Date;

  @ManyToOne(() => Category, (category) => category.products)
  category: Category;

  @ManyToMany(() => SubCategory, (subCategory) => subCategory.products, {
    cascade: true,
  })
  @JoinTable()
  subCategorys: SubCategory[];

  @ManyToMany(() => Size, (size) => size.products, { cascade: true })
  @JoinTable()
  sizes: Size[];

  @OneToMany(() => ReceiptDetail, (receiptDetail) => receiptDetail.product)
  receiptDetails: ReceiptDetail[];
}
