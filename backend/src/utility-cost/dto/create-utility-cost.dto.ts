import { User } from 'src/users/entities/user.entity';
import { UtilityDetail } from '../entities/uitlity-datail';

export class CreateUtilityCostDto {
  totalPrice: number;
  totalItem: number;
  user: User;
  ucDetails: UtilityDetail[];
}
