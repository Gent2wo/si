import { Test, TestingModule } from '@nestjs/testing';
import { UtilityCostController } from './utility-cost.controller';
import { UtilityCostService } from './utility-cost.service';

describe('UtilityCostController', () => {
  let controller: UtilityCostController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UtilityCostController],
      providers: [UtilityCostService],
    }).compile();

    controller = module.get<UtilityCostController>(UtilityCostController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
