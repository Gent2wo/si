import { Injectable } from '@nestjs/common';
import { CreateUtilityCostDto } from './dto/create-utility-cost.dto';
import { UpdateUtilityCostDto } from './dto/update-utility-cost.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UtilityDetail } from './entities/uitlity-datail';
import { UtilityCost } from './entities/utility-cost.entity';
import { User } from 'src/users/entities/user.entity';

@Injectable()
export class UtilityCostService {
  constructor(
    @InjectRepository(UtilityCost)
    private utilityCostRepository: Repository<UtilityCost>,
    @InjectRepository(UtilityDetail)
    private utilityDetailRepository: Repository<UtilityDetail>,
    @InjectRepository(User) private userRepository: Repository<User>,
  ) {}

  async create(createUtilityCostDto: CreateUtilityCostDto) {
    const uc = new UtilityCost();
    const user = await this.userRepository.findOneByOrFail({
      id: createUtilityCostDto.user.id,
    });
    uc.user = user;
    uc.totalItem = 0;
    uc.totalPrice = 0;
    uc.ucDetails = [];
    for (const ucd of createUtilityCostDto.ucDetails) {
      const ucItem = new UtilityDetail();
      ucItem.type = ucd.type;
      ucItem.price = ucd.price;
      ucItem.date = ucd.date;
      await this.utilityDetailRepository.save(ucItem);
      uc.ucDetails.push(ucItem);
      uc.totalItem += 1;
      uc.totalPrice = Number(uc.totalPrice) + Number(ucItem.price);
    }

    return await this.utilityCostRepository.save(uc);
  }

  findAll() {
    return this.utilityCostRepository.find({
      relations: ['user', 'ucDetails'],
    });
  }

  findOne(id: number) {
    return this.utilityCostRepository.findOneOrFail({
      where: {
        id: id,
      },
      relations: ['user', 'ucDetails'],
    });
  }

  update(id: number, updateUtilityCostDto: UpdateUtilityCostDto) {
    return `This action updates a #${id} utilityCost`;
  }

  async remove(id: number) {
    const delUC = await this.utilityCostRepository.findOneByOrFail({ id });
    await this.utilityCostRepository.remove(delUC);
    return delUC;
  }
}
