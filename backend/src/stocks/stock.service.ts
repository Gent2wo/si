import { Injectable } from '@nestjs/common';
import { CreateStockDto } from './dto/create-stock.dto';
import { UpdateStockDto } from './dto/update-stock.dto';
import { Repository } from 'typeorm';
import { Stock } from './entities/stock.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { StockDetail } from 'src/stockDetails/entities/stockDetail.entity';
import { User } from 'src/users/entities/user.entity';
import { Branch } from 'src/branches/entities/branch.entity';

@Injectable()
export class StockService {
  constructor(
    @InjectRepository(Stock) private stocksRepository: Repository<Stock>,
    @InjectRepository(StockDetail)
    private stockDetailsRepository: Repository<StockDetail>,
    @InjectRepository(User) private usersRepository: Repository<User>,
    @InjectRepository(Branch) private branchsRepository: Repository<Branch>,
  ) {}

  async create(createUserDto: CreateStockDto) {
    //create stock detail
    const stock = new Stock();
    try {
      const stockDetails: StockDetail[] = JSON.parse(
        JSON.stringify(createUserDto.stockDetails),
      );
      for (const stockdetail of stockDetails) {
        if (stockdetail.SD_ID > -1) {
          await this.stockDetailsRepository.update(
            stockdetail.SD_ID,
            stockdetail,
          );
        } else {
          await this.stockDetailsRepository.save(stockdetail);
        }
      }
      stock.S_STOCKDETAILS = stockDetails;
      const user = await this.usersRepository.findOneOrFail({
        where: { id: createUserDto.userId },
      });
      stock.S_USER = user;
      const branch = await this.branchsRepository.findOneOrFail({
        where: { id: createUserDto.branchId },
      });
      stock.S_BRANCH = branch;
      return this.stocksRepository.save(stock);
    } catch (error) {
      console.error('Error parsing stock details:', error);
    }
  }

  findAll() {
    return this.stocksRepository.find({
      relations: [
        'S_STOCKDETAILS',
        'S_STOCKDETAILS.SD_MATERIAL',
        'S_USER',
        'S_BRANCH',
      ],
    });
  }

  findOne(id: number) {
    return this.stocksRepository.findOne({
      where: { S_ID: id },
      relations: [
        'S_STOCKDETAILS',
        'S_STOCKDETAILS.SD_MATERIAL',
        'S_USER',
        'S_BRANCH',
      ],
    });
  }

  async update(id: number, updateStockDto: UpdateStockDto) {
    const updateStock = await this.stocksRepository.findOneOrFail({
      where: { S_ID: id },
      relations: [
        'S_STOCKDETAILS',
        'S_STOCKDETAILS.SD_MATERIAL',
        'S_USER',
        'S_BRANCH',
      ],
    });

    if (updateStock.S_USER.id !== updateStockDto.userId) {
      const user = await this.usersRepository.findOneOrFail({
        where: { id: updateStockDto.userId },
      });
      updateStock.S_USER = user;
    }

    if (updateStock.S_BRANCH.id !== updateStockDto.branchId) {
      const branch = await this.branchsRepository.findOneOrFail({
        where: { id: updateStockDto.branchId },
      });
      updateStock.S_BRANCH = branch;
    }

    const stockdetails: StockDetail[] = JSON.parse(
      JSON.stringify(updateStockDto.stockDetails),
    );
    // Update or create new Stock Details
    const updatedStockDetails = await Promise.all(
      stockdetails.map(async (detail) => {
        if (detail.SD_ID && detail.SD_ID > -1) {
          await this.stockDetailsRepository.update(detail.SD_ID, detail);
          return detail;
        } else {
          const newDetail = this.stockDetailsRepository.create(detail);
          return await this.stockDetailsRepository.save(newDetail);
        }
      }),
    );

    // Remove Stock Details not present in DTO
    const detailsToRemove = updateStock.S_STOCKDETAILS.filter(
      (detail) => !updatedStockDetails.find((d) => d.SD_ID === detail.SD_ID),
    );
    await Promise.all(
      detailsToRemove.map(async (detail) => {
        await this.stockDetailsRepository.remove(detail);
      }),
    );

    // Save the updated Stock entity
    await this.stocksRepository.save(updateStock);

    // Reload the updated Stock entity with relations
    const result = await this.stocksRepository.findOne({
      where: { S_ID: id },
      relations: ['S_STOCKDETAILS', 'S_BRANCH', 'S_USER'],
    });

    return result;
  }

  async remove(id: number) {
    try {
      // ค้นหา Stock ที่ต้องการลบพร้อมกับ StockDetails
      const deleteStock = await this.stocksRepository.findOneOrFail({
        where: { S_ID: id },
        relations: ['S_STOCKDETAILS', 'S_USER'],
      });

      //ต้องทำการลบ StockDetails ก่อนเพื่อหลีกเลี่ยงข้อผิดพลาดเกี่ยวกับคีย์นอก
      await Promise.all(
        deleteStock.S_STOCKDETAILS.map(async (stockDetail) => {
          await this.stockDetailsRepository.remove(stockDetail);
        }),
      );

      // จากนั้นลบ Stock ออกจากฐานข้อมูล
      await this.stocksRepository.remove(deleteStock);

      return deleteStock;
    } catch (error) {
      // ถ้าเกิดข้อผิดพลาดขึ้น
      console.error('Error occurred while deleting stock:', error);
      throw error; // สามารถปล่อยให้ caller จัดการต่อไป
    }
  }
}
