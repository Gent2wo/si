import type { StockDetail } from './StockDetail'

type Stock = {
  id: number
  date: string
  userID: number
  branchID: number
  stockDetails: StockDetail[]
}

export type { Stock }
