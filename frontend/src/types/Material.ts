type Material = {
  id: number
  name: string
  type: string
  minimum: number
  amount: number
}

export type { Material }
