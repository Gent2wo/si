import type { Branch } from "./Branch"

type Status = 'pay' | 'not pay'
type Salary = {
  id?: number
  fullName: string
  totalHour: number
  pricePerHour: number
  totalPay: number
  status: Status
  branch: Branch
  payDate: Date
  month: string
}
export type { Salary, Status }
