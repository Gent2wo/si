import type { Category } from './Category'
type details = 'Guest' | 'Member'
type status = 'Active' | 'Inactive'
type Promotion = {
  id?: number
  name: string
  discount: number
  startDate: Date
  endDate: Date
  details: string
  pointUse: number
  category: Category
  status: string
}
export type { Promotion, details, status }
