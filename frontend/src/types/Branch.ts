type Branch = {
  id?: number
  name: string
  address: string
  city: string
  province: string
  country: string
  postalcode: string
}

export type { Branch }
