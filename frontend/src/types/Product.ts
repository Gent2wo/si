import type { Type } from './Type'
import type { Category } from './Category'
import type { Size } from './Size'
import type { SubCategory } from './SubCategory'

type SweetLevel = '0' | '25' | '50' | '100'

type Product = {
  id?: number
  name: string
  price: number
  image: string
  unit?: string
  category: Category
  subCategorys?: SubCategory[]
  sweetLevel?: SweetLevel[]
  sizes?: Size[]
  size?: Size
  subCategory?: SubCategory
}

function getImageUrl(product: Product) {
  return `http://localhost:3000/images/products/${product.image}`
}

export { type Product, getImageUrl }
