type Member = {
  id?: number
  fristName: string
  lastName: string
  email?: string
  tel?: string
  birth?: Date
  point?: number
  pointRate?: number
  indate?: Date
}
export type { Member }
