type StockDetail = {
  id: number
  QOH: number
  balance: number
  materialId: number
  StockId: number
}

export type { StockDetail }
