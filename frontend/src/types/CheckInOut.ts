import type { User } from './User'

type status = 'Present' | 'Absent'
type CheckInOut = {
  id?: number
  email: string
  password: string
  fullName: string
  timeIn: Date
  timeOut: Date
  status: status
  totalHour: number
  date: Date
  userId?: number
  user?: User
}

export type { status, CheckInOut }
