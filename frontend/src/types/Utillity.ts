type Type = 'Water bill' | 'Electric bill' | 'Internet bill' | 'Phone bill'
type Utillity = {
  id?: number
  picture: string //ใส่รูป
  dateAdded: Date
  price: number
  type: Type
  detail: string
}

export type { Utillity, Type }
