import type { Material } from './Material'

const defaultCheckStockItem = {
  id: -1,
  remainingAmount: 0,
  totalUse: 0,
  materialId: -1,
  material: null
}

type CheckStockItem = {
  id: number
  remainingAmount: number
  totalUse: number
  materialId: number
  material?: Material
}
export { type CheckStockItem, defaultCheckStockItem }
