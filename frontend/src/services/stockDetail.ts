import type { StockDetail } from '../types/StockDetail'
import http from './http'

function addStockDetail(stockDetail: StockDetail) {
  const { id, ...stockdetailWithOutId } = stockDetail
  return http.post('/stockDetails', stockdetailWithOutId)
}
function updateStockDetail(stockDetail: StockDetail) {
  return http.patch(`/stockDetails/${stockDetail.id}`, stockDetail)
}
function delStockDetail(stockDetail: StockDetail) {
  return http.delete(`/stockDetails/${stockDetail.id}`)
}
function getStockDetail(id: number) {
  return http.get(`/stockDetails/${id}`)
}
function getStockDetails() {
  return http.get('/stockDetails')
}
function getStockDetailsByStockId(stockId: number) {
  return http.get(`/stockDetails/stockId/${stockId}`)
}

export default {
  addStockDetail,
  updateStockDetail,
  delStockDetail,
  getStockDetail,
  getStockDetails,
  getStockDetailsByStockId
}
