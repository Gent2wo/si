import http from './http'
import type { Salary } from '@/types/Salary'

function addSalary(salary: Salary) {
  return http.post('/salarys', salary)
}

function updateSalary(salary: Salary) {
  return http.patch(`/salarys/${salary.id}`, { status: salary.status })
}

function delSalary(salary: Salary) {
  return http.delete(`/salarys/${salary.id}`)
}

function getSalary(id: number) {
  return http.get(`/salarys/${id}`)
}

function getSalarysByBranch(branchId: number) {
  return http.get(`/salarys/branch/${branchId}`)
}

function getSalarys() {
  return http.get('/salarys')
}

export default { addSalary, updateSalary, delSalary, getSalary, getSalarys, getSalarysByBranch }
