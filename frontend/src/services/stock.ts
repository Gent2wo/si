import type { Stock } from '../types/Stock'
import http from './http'

function addStock(stock: Stock) {
  // ลบ id และ date ออกจาก object stock
  const { id, date, stockDetails, ...rest } = stock

  // ปรับโครงสร้าง stockDetails
  const newStockDetails = stockDetails.map(({ id, StockId, QOH, balance, materialId }) => ({
    SD_QOH: QOH,
    SD_BALANCE: balance,
    SD_MATERIAL: materialId
  }))

  // สร้าง object ใหม่สำหรับส่งข้อมูล
  const dataToSend = {
    ...rest, // เพิ่มส่วนที่เหลือจาก stock หลังจากลบ id และ date
    stockDetails: newStockDetails
  }

  console.log('Data to send: ', dataToSend)
  return http.post('/stocks', dataToSend)
}

function updateStock(stock: Stock) {
  return http.patch(`/stocks/${stock.id}`, stock)
}
function delStock(stock: Stock) {
  return http.delete(`/stocks/${stock.id}`)
}
function getStock(id: number) {
  return http.get(`/stocks/${id}`)
}
function getStocks() {
  return http.get('/stocks')
}

export default { addStock, updateStock, delStock, getStock, getStocks }
