import http from './http'
import type { Category } from '@/types/Category'

function addCategory(category: Category) {
  return http.post('/categorys', category)
}
function updateCategory(category: Category) {
  return http.patch(`/categorys/${category.id}`, category)
}
function delCategory(category: Category) {
  return http.delete(`/categorys/${category.id}`)
}
function getCategory(id: number) {
  return http.get(`/categorys/${id}`)
}
function getCategorys() {
  return http.get('/categorys')
}

export default { addCategory, updateCategory, delCategory, getCategory, getCategorys }
