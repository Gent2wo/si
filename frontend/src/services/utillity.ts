import type { Utillity } from '@/types/Utillity'
import http from './http'

function addUtillity(utillity: Utillity & { files: File[] }) {
  const formData = new FormData()
  formData.append('type', utillity.type)
  formData.append('price', utillity.price.toString())
  formData.append('detail', utillity.detail)
  if (utillity.files && utillity.files.length > 0) formData.append('file', utillity.files[0])
  return http.post('/utillites', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function updateUtillity(utillity: Utillity & { files: File[] }) {
  const formData = new FormData()
  formData.append('type', utillity.type)
  formData.append('price', utillity.price.toString())
  formData.append('detail', utillity.detail)
  if (utillity.files && utillity.files.length > 0) formData.append('file', utillity.files[0])
  return http.post(`/utillites/${utillity.id}`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function deleteUtillity(utillity: Utillity) {
  return http.delete(`/utillites/${utillity.id}`)
}

function getUtillity(id: number) {
  return http.get(`/utillites/${id}`)
}

function getUtillities() {
  return http.get('/utillites')
}

export default { addUtillity, updateUtillity, deleteUtillity, getUtillity, getUtillities }
