import http from './http'
import type { Promotion } from '@/types/Promotion'

function addPromotion(promotion: Promotion) {
  return http.post('/promotion', promotion)
}

function updatePromotion(promotion: Promotion) {
  return http.patch(`/promotion/${promotion.id}`, promotion)
}

function delPromotion(promotion: Promotion) {
  return http.delete(`/promotion/${promotion.id}`)
}

function getPromotion(id: number) {
  return http.get(`/promotion/${id}`)
}

function getPromotions() {
  return http.get('/promotion')
}

export default { addPromotion, updatePromotion, delPromotion, getPromotion, getPromotions }
