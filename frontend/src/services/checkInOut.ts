import type { CheckInOut } from '@/types/CheckInOut'
import http from './http'

function addCheckInOut(checkInOut: CheckInOut) {
  return http.post('/checkInOuts', checkInOut)
}

function updateCheckInOut(checkInOut: CheckInOut) {
  return http.patch(`/checkInOuts/${checkInOut.id}`, checkInOut)
}

function delCheckInOut(checkInOut: CheckInOut) {
  return http.delete(`/checkInOuts/${checkInOut.id}`)
}

function getCheckInOut(id: number) {
  return http.get(`/checkInOuts/${id}`)
}

function getCheckInOuts() {
  return http.get('/checkInOuts')
}

export default { addCheckInOut, updateCheckInOut, delCheckInOut, getCheckInOut, getCheckInOuts }
