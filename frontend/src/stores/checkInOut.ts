import { ref, computed, nextTick, reactive } from 'vue'
import { defineStore } from 'pinia'
import type { CheckInOut } from '@/types/CheckInOut'
import checkInOutService from '@/services/checkInOut'
import userService from '@/services/user'
import type { User } from '@/types/User'
import { useLoadingStore } from './loading'
import { useMessageStore } from './message'
import checkInOut from '@/services/checkInOut'
import { useUserStore } from './user'
import { useAuthStore } from './auth'
import { useRouter } from 'vue-router'

export const useCheckInOutStore = defineStore('checkinout', () => {
  const authStore = useAuthStore()
  const userStore = useUserStore()
  const checkinout = ref<CheckInOut>()
  const checkInOuts = ref<CheckInOut[]>([])
  const dialog = ref(false)
  const loading = ref(false)
  const dialogDelete = ref(false)
  const loginDialog = ref(false)
  const logoutDialog = ref(false)
  const from = ref(false)
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const initialCheckinout: CheckInOut = {
    email: '',
    password: '',
    fullName: '',
    timeIn: new Date(),
    timeOut: new Date(),
    status: 'Absent',
    totalHour: 0,
    date: new Date(),
    // userId: authStore.getCurrentUser()?.id!,
    // user: userStore.getCurrentUser()!
  }

  const editedCheckinout = ref<CheckInOut>(JSON.parse(JSON.stringify(initialCheckinout)))
  let editedIndex = -1
  // let lastId = 7
  // let lastEmId = 4

  async function getCheckInOuts() {
    loadingStore.doLoad()
    const res = await checkInOutService.getCheckInOuts()
    checkInOuts.value = res.data
    loadingStore.finish()
  }
  async function save() {
    await saveCheckinout()
    closeDialog()
  }


  async function saveCheckinout() {
    try {
      loadingStore.doLoad()
      const currentTimeInThaiTimeZone = new Date().toLocaleString('en-us', {
        timeZone: 'Asia/Bangkok'
      })
      editedCheckinout.value.timeOut = new Date(currentTimeInThaiTimeZone)
      const checkInout = editedCheckinout.value
      if (!checkInout.id) {
        // Add new
        console.log('Post ' + JSON.stringify(checkInout))
        const res = await checkInOutService.addCheckInOut(checkInout)
      } else {
        console.log('Patch ' + JSON.stringify(checkInout))
        const res = await checkInOutService.updateCheckInOut(checkInout)
      }

      await getCheckInOuts()
      loadingStore.finish()
    } catch (e: any) {
      loadingStore.finish()
      messageStore.showMessage(e.message)
    }
  }

  function closeDialog() {
    loginDialog.value = false
    logoutDialog.value = false
    nextTick(() => {
      clearForm()
    })
  }

  function closeDelete() {
    dialogDelete.value = false
    clearForm()
  }

  async function deleteItemConfirm() {
    await deleteCheckInOut()
    closeDelete()
  }

  async function deleteCheckInOut() {
    loadingStore.doLoad()
    const checkInOut = editedCheckinout.value
    const res = await checkInOutService.delCheckInOut(checkInOut)
    clearForm()
    await getCheckInOuts()
    loadingStore.finish()
  }

  async function deleteItem(item: CheckInOut) {
    if (!item.id) return
    await getCheckInOut(item.id)
    dialogDelete.value = true
    editedIndex = -1
  }

  async function getCheckInOut(id: number) {
    loadingStore.doLoad()
    const res = await checkInOutService.getCheckInOut(id)
    editedCheckinout.value = res.data
    loadingStore.finish()
  }
  function onSubmit() { }
  function close() {
    dialog.value = false
    nextTick(() => {
      editedCheckinout.value = Object.assign({}, initialCheckinout)
      editedIndex = -1
    })
  }

  async function editItem(item: CheckInOut) {
    if (!item.id) return
    await getCheckInOut(item.id)
    dialog.value = true
    editedIndex = -1
  }

  function clearForm() {
    editedCheckinout.value = JSON.parse(JSON.stringify(initialCheckinout))
  }

  // function calculateTotalHours(timeIn: Date, timeOut: Date): number {
  //   // หาความแตกต่างระหว่างเวลาออกงานและเวลาเข้างานในรูปแบบ milliseconds
  //   const differenceMs = timeOut.getTime() - timeIn.getTime();
  //   // แปลงค่า milliseconds เป็นชั่วโมงและตัดเศษทิ้ง
  //   const totalHours = Math.floor(differenceMs / (1000 * 60 * 60));
  //   return totalHours;
  // }

  // // ตัวอย่างการใช้งาน
  // const timeIn = new Date('2024-03-31T08:00:00'); // วันที่และเวลาเข้างาน
  // const timeOut = new Date('2024-03-31T17:30:00'); // วันที่และเวลาออกงาน
  // const totalHours = calculateTotalHours(timeIn, timeOut);
  // console.log(totalHours); // จะได้ค่าชั่วโมงที่ลบกันแล้วเศษทิ้ง


  // const logIn = async function (email: string, password: string) {
  //   // loadingStore.doLoad()
  //   try {
  //     const res = await userService.login(email, password)
  //     console.log(res.data)
  //     localStorage.setItem('user', JSON.stringify(res.data.user))
  //     messageStore.showMessage('Login Success')
  //     router.replace('/')
  //   } catch (e: any) {
  //     console.log(e)
  //     messageStore.showMessage(e.message)
  //   }
  //   // loadingStore.finish()
  // }
  // async function getCurrentUser(): Promise<User | null> {
  //   const strUser = await getCheckInOuts(); // Assuming getCheckInOuts returns a Promise<User | null>
  //   if (strUser === null) return null; // Check if the resolved value of the Promise is null
  //   return strUser;
  // }

  return {
    dialog,
    dialogDelete,
    loginDialog,
    checkInOuts,
    from,
    editedCheckinout,
    loading,
    logoutDialog,
    save,
    closeDialog,
    closeDelete,
    deleteItemConfirm,
    deleteItem,
    onSubmit,
    close,
    deleteCheckInOut,
    getCheckInOuts,
    saveCheckinout,
    clearForm,
    editItem,
    getCheckInOut,
    checkinout
    // logIn,
    // getCurrentUser
  }
})
