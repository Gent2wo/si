import { useLoadingStore } from './loading'
import { ref, computed, nextTick } from 'vue'
import { defineStore } from 'pinia'
import branchService from '@/services/branch'
import type { Branch } from '@/types/Branch'

export const useBranchStore = defineStore('branch', () => {
  const loadingStore = useLoadingStore()
  const branches = ref<Branch[]>([])
  const loading = ref(false)
  const dialog = ref(false)
  const newBranchdialog = ref(false)
  const dialogDelete = ref(false)
  const from = ref(false)
  const initialBranch: Branch = {
    name: '',
    address: '180 หมู่ 2 ต.บางแสน',
    city: 'บางแสน',
    province: 'ชลบุรี',
    country: 'ไทย',
    postalcode: '20131'
  }
  const editedBranch = ref<Branch>(JSON.parse(JSON.stringify(initialBranch)))
  let editedIndex = -1
  async function getBranch(id: number) {
    loadingStore.doLoad()
    const res = await branchService.getBranch(id)
    console.log(res.data)
    const branchDB = {
      id: res.data.BRANCH_ID,
      name: res.data.BRANCH_NAME,
      address: res.data.BRANCH_ADDRESS,
      city: res.data.BRANCH_CITY,
      province: res.data.BRANCH_PROVINCE,
      country: res.data.BRANCH_COUNTRY,
      postalcode: res.data.BRANCH_POSTAL_CODE
    }
    editedBranch.value = branchDB
    console.log('editedBranch', editedBranch.value)
    loadingStore.finish()
  }

  // const headers = [
  //   {
  //     title: 'Id',
  //     key: 'id',
  //     value: 'id'
  //   },
  //   {
  //     title: 'Name',
  //     key: 'name',
  //     value: 'name'
  //   },
  //   {
  //     title: 'Address',
  //     key: 'address',
  //     value: 'address'
  //   },
  //   {
  //     title: 'City',
  //     key: 'city',
  //     value: 'city'
  //   },
  //   {
  //     title: 'Povince',
  //     key: 'province',
  //     value: 'province'
  //   },
  //   {
  //     title: 'Country',
  //     key: 'country',
  //     value: 'country'
  //   },
  //   {
  //     title: 'Postalcode',
  //     key: 'postalcode',
  //     value: 'postalcode'
  //   },
  //   {
  //     title: 'Actions',
  //     key: 'actions',
  //     sortable: false
  //   }
  // ]

  async function save() {
    await saveBranch()
    closeDialog()
  }

  async function getBranches() {
    try {
      loadingStore.doLoad()
      const res = await branchService.getBranches()
      if (Array.isArray(res.data)) {
        const branchDB = res.data.map((dbBranch: any) => {
          console.log('dbBranch', dbBranch)
          console.log('dbBranch Id', dbBranch.BRANCH_ID)
          return {
            id: dbBranch.BRANCH_ID,
            name: dbBranch.BRANCH_NAME,
            address: dbBranch.BRANCH_ADDRESS,
            city: dbBranch.BRANCH_CITY,
            province: dbBranch.BRANCH_PROVINCE,
            country: dbBranch.BRANCH_COUNTRY,
            postalcode: dbBranch.BRANCH_POSTAL_CODE
          }
        })
        branches.value = branchDB
      }
      loadingStore.finish()
    } catch (error) {
      console.error('Error:', error)
      loadingStore.finish()
    }
  }

  async function saveBranch() {
    loadingStore.doLoad()
    const branch = editedBranch.value
    if (!branch.id) {
      // Add new
      console.log('Post ' + JSON.stringify(branch))
      const res = await branchService.addBranch(branch)
    } else {
      // Update
      console.log('Patch ' + JSON.stringify(branch))
      const res = await branchService.updateBranch(branch)
    }
    clearForm()
    await getBranches()
    loadingStore.finish()
  }
  async function deleteBranch() {
    loadingStore.doLoad()
    const branch = editedBranch.value
    const res = await branchService.delBranch(branch)
    clearForm()
    await getBranches()
    loadingStore.finish()
  }

  async function editItem(item: Branch) {
    if (!item.id) return
    await getBranch(item.id)
    dialog.value = true
    editedIndex = -1
  }

  function onSubmit() {}

  function close() {
    dialog.value = false
    nextTick(() => {
      editedBranch.value = Object.assign({}, initialBranch)
      editedIndex = -1
    })
  }

  async function deleteItem(item: Branch) {
    if (!item.id) return
    await getBranch(item.id)
    dialogDelete.value = true
    editedIndex = -1
  }

  function shownewDialog() {
    dialog.value = true
  }

  async function deleteItemConfirm() {
    await deleteBranch()
    closeDelete()
  }

  function closeDialog() {
    dialog.value = false
    clearForm()
  }

  function closeDelete() {
    dialogDelete.value = false
    clearForm()
  }

  function clearForm() {
    editedBranch.value = JSON.parse(JSON.stringify(initialBranch))
  }
  return {
    branches,
    getBranches,
    saveBranch,
    deleteBranch,
    editedBranch,
    getBranch,
    clearForm,
    shownewDialog,
    deleteItemConfirm,
    deleteItem,
    close,
    onSubmit,
    editItem,
    save,
    editedIndex,
    from,
    loading,
    newBranchdialog,
    dialog,
    closeDialog,
    dialogDelete,
    closeDelete
  }
})
