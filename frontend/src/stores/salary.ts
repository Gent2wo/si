import type { Salary } from '@/types/Salary'
import { nextTick, ref } from 'vue'
import { useLoadingStore } from './loading'
import salaryService from '@/services/salary'
import { useMessageStore } from './message'
import router from '@/router'

export function useSalaryStore() {
  const PaymentDialog = ref(false)
  const dialog = ref(false)
  const dialogDelete = ref(false)
  const SlipDialog = ref(false)
  const form = ref(false)
  const newUser = ref(false)

  const salarys = ref<Salary[]>([])

  const initialSalary: Salary = {
    fullName: '',
    totalHour: 0,
    pricePerHour: 0,
    totalPay: 0,
    status: 'not pay',
    branch: {
      id: 0,
      name: '',
      address: '',
      city: '',
      province: '',
      country: '',
      postalcode: ''
    },
    payDate: new Date(),
    month: ''
  }

  const editedSalary = ref<Salary>(JSON.parse(JSON.stringify(initialSalary)))
  let editedIndex = -1
  // let lastId = 4

  const messageStore = useMessageStore()
  const loadindStore = useLoadingStore()
  const loading = ref(false)

  const selectedSalary = ref<Salary>(JSON.parse(JSON.stringify(initialSalary)))

  const selected = ref<Salary>({
    fullName: '',
    totalHour: 0,
    pricePerHour: 0,
    totalPay: 0,
    status: 'not pay',
    branch: {
      id: 0,
      name: '',
      address: '',
      city: '',
      province: '',
      country: '',
      postalcode: ''
    },
    payDate: new Date(),
    month: ''
  })

  async function getSalarys() {
    loadindStore.doLoad()
    const res = await salaryService.getSalarys()
    const salaryData = res.data.map((salary: any) => {
      return {
        id: salary.id,
        fullName: salary.fullName,
        totalHour: salary.totalHour,
        pricePerHour: salary.pricePerHour,
        totalPay: salary.totalPay,
        status: salary.status,
        branch: salary.branch.id,
        payDate: salary.payDate,
        month: salary.month
      }
    })
    salarys.value = salaryData
    loadindStore.finish()
  }

  async function save() {
    await saveSalary()
    closeDialog()
  }

  async function saveSalary() {
    try {
      loadindStore.doLoad()
      const salary = editedSalary.value
      if (!salary.id) {
        console.log('Post' + JSON.stringify(salary))
        const res = await salaryService.addSalary(salary)
      } else {
        console.log('Patch' + JSON.stringify(salary))
        const res = await salaryService.updateSalary(salary)
      }

      clearForm()
      await getSalarys()
      loadindStore.finish()
    } catch (e: any) {
      loadindStore.finish()
      messageStore.showMessage(e.message)
    }
  }

  function closeDialog() {
    dialog.value = false
    clearForm()
  }

  function closeDelete() {
    dialogDelete.value = false
    clearForm()
  }

  async function deleteItemConfirm() {
    await deleteSalary()
    closeDelete()
  }

  async function deleteSalary() {
    loadindStore.doLoad()
    const salary = editedSalary.value
    const res = await salaryService.delSalary(salary)
    clearForm()
    await getSalarys()
    loadindStore.finish()
  }

  async function deleteItem(item: Salary) {
    if (!item.id) return
    await getSalary(item.id)
    dialogDelete.value = true
    editedIndex = -1
  }

  function onSubmit() {}

  function close() {
    dialog.value = false
    nextTick(() => {
      editedSalary.value = Object.assign({}, initialSalary)
      editedIndex = -1
    })
  }

  async function editItem(item: Salary) {
    if (!item.id) return
    await getSalary(item.id)
    dialog.value = true
    editedIndex = -1
  }

  async function getSalary(id: number) {
    loadindStore.doLoad()
    const res = await salaryService.getSalary(id)
    editedSalary.value = res.data
    loadindStore.finish()
  }

  function clearForm() {
    editedSalary.value = JSON.parse(JSON.stringify(initialSalary))
  }

  const showPaymentDialog = (item: Salary) => {
    if (item.status == 'not pay') {
      selected.value = item
      // PaymentDialog.value = true
      router.push('/salary/payrollView')

      console.log('1:', selected)
      console.log('2:', item)
    }
  }

  const calculateSalary = () => {
    if (selected.value) {
      const { totalHour, pricePerHour } = selected.value
      return totalHour * pricePerHour
    }
    return 0
  }

  // Inside useSalaryStore
  const confirmPayment = async () => {
    try {
      if (selected.value.status === 'not pay') {
        // Update the status to 'pay'
        selected.value.status = 'pay'

        // Send an update request to the server
        await salaryService.updateSalary(selected.value)

        // Update the salary in the local array (optional, depending on your architecture)
        const indexToUpdate = salarys.value.findIndex((item) => item.id === selected.value.id)
        if (indexToUpdate !== -1) {
          salarys.value[indexToUpdate] = { ...selected.value }
        }
      }
    } catch (error) {
      // Handle errors, show a message, or log the error
      console.error('Error confirming payment:', error)
    }
  }

  return {
    PaymentDialog,
    dialog,
    SlipDialog,
    dialogDelete,
    form,
    salarys,
    editedSalary,
    selected,
    loading,
    initialSalary,
    selectedSalary,
    showPaymentDialog,
    calculateSalary,
    confirmPayment,
    getSalarys,
    save,
    saveSalary,
    closeDialog,
    closeDelete,
    deleteItemConfirm,
    deleteSalary,
    deleteItem,
    onSubmit,
    close,
    editItem,
    getSalary,
    clearForm,
    newUser
  }
}
