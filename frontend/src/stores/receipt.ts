import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'

import type { ReceiptItem } from '@/types/ReceiptItem'
import type { Product } from '@/types/Product'
import type { Receipt } from '@/types/Receipt'
import { useMemberStore } from './member'
import { usePromotionStore } from './promotion'
import type { Promotion } from '@/types/Promotion'
import receiptService from '@/services/receipt'
import { useAuthStore } from './auth'

export const useReceiptStore = defineStore('receipt', () => {
  const promotionStore = usePromotionStore()
  const memberStore = useMemberStore()
  const authStore = useAuthStore()
  const receiptDialog = ref(false)
  const QRpaymentDialog = ref(false)
  const receipt = ref<Receipt>()
  const receiptItems = ref<ReceiptItem[]>([])
  const selectPaymentOption = ref('cash')
  initReceipt()
  function initReceipt() {
    receipt.value = {
      id: 0,
      createdDate: new Date(),
      totalPrice: 0,
      discount: 0,
      income: 0,
      qty: 0,
      change: 0,
      paymentType: 'cash',
      userId: authStore.getCurrentUser()!.id!,
      memberId: memberStore.getCurrentMember()?.id!,
      branchId: authStore.getCurrentUser()!.branch.id!,
      promotionId: 0,
      member: memberStore.getCurrentMember()!,
      user: authStore.getCurrentUser()!
    }
    receiptItems.value = []
  }

  watch(
    receiptItems,
    () => {
      calReceipt()
    },
    { deep: true }
  )
  const paymentTypeDialog = ref(false)

  const addReceiptItem = (newReceiptItem: ReceiptItem) => {
    receiptItems.value.push(newReceiptItem)
  }

  const removeReceiptItem = (selectedItem: ReceiptItem) => {
    const index = receiptItems.value.findIndex((item) => item === selectedItem)
    receiptItems.value.splice(index, 1)
  }
  const inc = (selectedItem: ReceiptItem) => {
    selectedItem.unit++
  }
  const dec = (selectedItem: ReceiptItem) => {
    selectedItem.unit--
    if (selectedItem.unit === 0) {
      removeReceiptItem(selectedItem)
    }
  }
  const removeItem = (item: ReceiptItem) => {
    const index = receiptItems.value.findIndex((ri) => ri === item)
    receiptItems.value.splice(index, 1)
  }

  function updateDiscount(discount: number) {
    // receipt.value!.promotionDiscount = discount
    receipt.value!.discount = discount
  }

  function calPro(totalBefore: number, discount: number) {
    if (discount > 0) {
      receipt.value!.totalPrice = totalBefore - discount
    } else {
      receipt.value!.totalPrice = totalBefore
    }
    console.log('+++ ' + discount)

    console.log('After ' + receipt.value!.totalPrice)
  }

  const calReceipt = function () {
    let totalBefore = 0
    let totalDiscount = 0
    let totalQty = 0

    for (let i = 0; i < receiptItems.value.length; i++) {
      const item = receiptItems.value[i]
      totalBefore += item.price * item.unit
      totalQty += item.unit
    }

    if (memberStore.getCurrentMember()) {
      totalDiscount = totalBefore * 0.15 // Assuming 15% discount for members
    }

    receipt.value!.totalPrice = totalBefore - totalDiscount
    receipt.value!.discount = totalDiscount
    receipt.value!.qty = totalQty

    // If you have more calculations such as income and change, uncomment and add them here

    // receipt.value.income = ...;
    // receipt.value.change = ...;

    // Adjustments for member or non-member
    // if (memberStore.currentMember) {
    //     receipt.value.total = totalBefore * 0.85;
    //     receipt.value.memberDiscount = totalBefore - receipt.value.total;
    // } else {
    //     receipt.value.total = totalBefore;
    // }

    // receipt.value.totalMember = receipt.value.total;
  }

  // function calReceipt() {
  //   const currentDiscount = discount.value // เก็บค่าล่าสุดจาก ref

  //   updateDiscount(currentDiscount)
  //   console.log('calReceipt is called')
  //   let totalBefore = 0

  //   for (const item of receiptItems.value) {
  //     totalBefore += item.price * item.unit
  //   }

  //   receipt.value!.totalPrice = totalBefore

  // if (memberStore.currentMember) {
  //   receipt.value.total = totalBefore * 0.85
  //   receipt.value.memberDiscount = totalBefore - receipt.value.total
  // } else {
  //   receipt.value.total = totalBefore
  // }
  // receipt.value.totalMember = receipt.value.total

  /////////////// OLD MAKMAK
  // console.log('Before Pro ' + receipt.value.total);
  // console.log('discount ' + currentDiscount); // ใช้ค่าที่เก็บไว้จาก ref

  // calPro(totalBefore, currentDiscount); // ส่งค่าที่เก็บไว้จาก ref
  // }

  // function calReceipt() {
  //   const currentDiscount = discount.value; // เก็บค่าล่าสุดจาก ref

  //   updateDiscount(currentDiscount);
  //   console.log('calReceipt is called');
  //   let totalBefore = 0;
  //   let memDiscount = 0;
  //   let proDiscount = 0;

  //   for (const item of receiptItems.value) {
  //     totalBefore += item.price * item.unit;
  //   }

  //   receipt.value.totalBefore = totalBefore;

  //   if (memberStore.currentMember) {
  //     memDiscount = totalBefore * 0.85
  //     receipt.value.memberDiscount = totalBefore - memDiscount
  //     console.log("member discount",receipt.value.memberDiscount)
  //   } else {
  //     memDiscount = totalBefore
  //   }

  //   if(promotionStore.selectedPromotions){
  //     proDiscount = totalBefore - currentDiscount
  //     receipt.value.promotionDiscount = totalBefore - proDiscount
  //     console.log("pro discount",receipt.value.promotionDiscount)
  //   }else{
  //     proDiscount = totalBefore
  //   }

  //   receipt.value.total = totalBefore - (memDiscount - proDiscount)

  //   // receipt.value.totalMember = receipt.value.total;
  //   // console.log('Before Pro ' + receipt.value.total);
  //   // console.log('discount ' + currentDiscount); // ใช้ค่าที่เก็บไว้จาก ref

  //   calPro(totalBefore, proDiscount); // ส่งค่าที่เก็บไว้จาก ref
  // }
  // Callback function to update discount from usePromotionStore

  function showPaymentDialog() {
    paymentTypeDialog.value = true
  }

  function showReceiptDialog() {
    receipt.value!.receiptItems = receiptItems.value
    receiptDialog.value = true
  }
  function clear() {
    receiptItems.value = []
    receipt.value = {
      id: 0,
      createdDate: new Date(),
      totalPrice: 0,
      discount: 0,
      income: 0,
      qty: 0,
      change: 0,
      paymentType: 'cash',
      userId: 0,
      memberId: memberStore.getCurrentMember()!.id!,
      branchId: 0,
      promotionId: 0,
      member: memberStore.getCurrentMember()!
    }
    promotionStore.resetSelectedPromotions()
    memberStore.clear()
  }
  const receiptOrder = async () => {
    try {
      // ตรวจสอบว่ามีข้อมูลสมาชิกหรือไม่
      if (receipt.value?.member) {
        const memberId = receipt.value.member.id
        receipt.value!.memberId = memberId !== undefined ? memberId : -1
      } else {
        receipt.value!.memberId = -1
      }
  
      // ตรวจสอบว่ามีข้อมูลผู้ใช้หรือไม่
      if (receipt.value?.user) {
        const userId = receipt.value.user.id
        receipt.value!.userId = userId !== undefined ? userId : -1
      } else {
        receipt.value!.userId = -1
      }
  
      console.log('hi '+ JSON.stringify(receipt.value))
      // เรียกใช้งานฟังก์ชัน addReceiptOrder เพื่อเพิ่มใบเสร็จใหม่
      await receiptService.addReceiptOrder(receipt.value!, receiptItems.value)
  
      // ล้างค่าใบเสร็จใหม่
      initReceipt()

      console.log(receipt)
      console.log(receiptItems)
    } catch (e) {
      console.error('Error:', e)
    }
  }
  

  return {
    receiptItems,
    receipt,
    receiptDialog,
    QRpaymentDialog,
    receiptOrder,
    showPaymentDialog,
    showReceiptDialog,
    paymentTypeDialog,
    addReceiptItem,
    removeReceiptItem,
    inc,
    dec,
    removeItem,
    calReceipt,
    updateDiscount,
    calPro,
    clear,
    selectPaymentOption
  }
})
